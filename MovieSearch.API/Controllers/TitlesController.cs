﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using MovieSearch.Core;
using MovieSearch.Core.Entities;
using MovieSearch.Core.Interfaces;
using MovieSearch.Core.Models;

namespace MovieSearch.API.Controllers
{
    //set attribute to enable request traffic (CORS) from AngularJS front end
    [EnableCors(origins: "http://localhost:32891", headers: "*", methods: "*")]
    public class TitlesController : ApiController
    {
        private ITitleRepository repository;

        //inject repository interface into Controller constructor for DI
        public TitlesController(ITitleRepository titleRepository)
        {
            repository = titleRepository;
        }

        // GET: api/Titles
        public IEnumerable<TitleModel> GetTitles()
        {
            //get MovieSearch data from Title Repository
            return repository.GetTitles().ToList();
        }

        

       
    }
}