﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieSearch.Core.Entities;
using MovieSearch.Core.Models;

namespace MovieSearch.Core.Interfaces
{
    public interface ITitleRepository
    {
        IEnumerable<TitleModel> GetTitles(); 
    }
}
