﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieSearch.Core.Entities;

namespace MovieSearch.Core.Models
{
    public class TitleModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int? ReleaseYear { get; set; }
        public string StoryLineDescription { get; set; }
        public List<string> Director { get; set; }
        public List<string> Actors { get; set; }
        public List<string> Genres { get; set; }
    }
}
