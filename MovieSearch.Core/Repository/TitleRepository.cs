﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieSearch.Core;
using MovieSearch.Core.Entities;
using MovieSearch.Core.Interfaces;
using System.Data.Entity;
using MovieSearch.Core.Models;

namespace MovieSearch.Core.Repository
{
    public class TitleRepository : ITitleRepository
    {
        //declare db connection context
        EFContext context = new EFContext();
        
        public IEnumerable<TitleModel> GetTitles()
        {
            //get movie info from Title, StoryLine, TitleParticipant, and Participant tables
            var titles = (from t in context.Titles
                          join sl in context.StoryLines on t.TitleId equals sl.TitleId
                          where sl.Type == "Turner External"
                          select new 
                          {
                              StoryLineDescription = sl.Description,
                              TitleName = t.TitleName,
                              ReleaseYear = t.ReleaseYear,
                              TitleParticipants = t.TitleParticipants,
                              TitleId = t.TitleId
                          })
                          .AsEnumerable()
                          .Select(obj => new TitleModel
                          { 
                              TitleId = obj.TitleId,
                              StoryLineDescription = obj.StoryLineDescription,
                              TitleName = obj.TitleName,
                              ReleaseYear = obj.ReleaseYear,
                              Director = (from tp in context.TitleParticipants
                                          join p in context.Participants on tp.ParticipantId equals p.Id
                                          where tp.RoleType == "Director" && tp.TitleId == obj.TitleId
                                          orderby tp.IsKey descending
                                          select tp.Participant.Name)
                                          .ToList(),
                              Actors = (from tp in context.TitleParticipants
                                          join p in context.Participants on tp.ParticipantId equals p.Id
                                          where tp.RoleType == "Actor" && tp.TitleId == obj.TitleId
                                          orderby tp.IsKey descending
                                          select tp.Participant.Name)
                                          .ToList(),
                              Genres = (from tg in context.TitleGenres
                                          join g in context.Genres on tg.GenreId equals g.Id
                                          where tg.TitleId == obj.TitleId
                                          select g.Name)
                                          .ToList()
                              
                          });
            //return combined result set
            return titles;

        }
    }
    
}
