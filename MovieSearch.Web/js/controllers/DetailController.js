﻿app.controller('DetailController', ['$scope', 'titles', '$routeParams', function ($scope, titles, $routeParams) {
    titles.success(function (data) {
        //match up the titleId in the JSON response and assign it to $scope
        angular.forEach(data, function (item) {
            if (item.TitleId == $routeParams.titleId)
                $scope.title = item;   
        });
    });
}]);