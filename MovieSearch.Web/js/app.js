﻿var app = angular.module('MovieSearchApp', ['ngRoute']);
app.config(function ($routeProvider) {
      $routeProvider
        .when('/moviesearch', {
            controller: 'TitleController',
            templateUrl: 'js/views/titles.html'
        })
        .when('/moviesearch/:titleId', {
            controller: 'DetailController',
            templateUrl: 'js/views/details.html'
        })
        .otherwise({
            redirectTo: '/moviesearch'
        });
});
